Str = """
  //
  // 1th dword
  //
  UINT8  BlockEraseSizes : 2;
  UINT8  WriteGranularity : 1;
  UINT8  VolatitleStatusBit : 1;
  UINT8  WriteEnableOpcodeSelect : 1;
  UINT8  Reserve0 : 3;
  UINT8  EraseOpcode;
  UINT8  FastRead112Support : 1;   
  UINT8  AddressBytes : 2;         
  UINT8  DTRSupport : 1;           
  UINT8  FastRead122Support : 1;
  UINT8  FastRead144Support : 1;
  UINT8  FastRead114Support : 1;
  UINT8  Reserve1 : 1;
  UINT8  Reserve2;
  //
  // 2nd dword
  //
  UINT32 FlashMemoryDensity;
  //
  // 3rd dword
  //
  UINT8  FastRead144DummyCycle : 5;
  UINT8  FastRead144ModeBit : 3;
  UINT8  FastRead144OpCode;
  UINT8  FastRead114DummyCycle : 5;
  UINT8  FastRead114ModeBit : 3;
  UINT8  FastRead114OpCode;
  //
  // 4th dword
  //
  UINT8  FastRead112DummyCycle : 5;
  UINT8  FastRead112ModeBit : 3;
  UINT8  FastRead112OpCode;
  UINT8  FastRead122DummyCycle : 5;
  UINT8  FastRead122ModeBit : 3;
  UINT8  FastRead122OpCode;
  //
  // 5th dword
  //
  UINT8  FastRead222Support : 1;
  UINT8  Reserve3 : 3;
  UINT8  FastRead444Support : 1;
  UINT8  Reserve4 : 3;
  UINT8  Reserve5[3];
  //
  // 6th dword
  //
  UINT8  Reserve6[2];
  UINT8  FastRead222DummyCycle : 5;
  UINT8  FastRead222ModeBit : 3;
  UINT8  FastRead222OpCode;
  //
  // 7th dword
  //
  UINT8  Reserve7[2];
  UINT8  FastRead444DummyCycle : 5;
  UINT8  FastRead444ModeBit : 3;
  UINT8  FastRead444OpCode;
  //
  // 8th dword
  //
  UINT8  SectorType1Size;
  UINT8  SectorType1EraseOpcode;
  UINT8  SectorType2Size;
  UINT8  SectorType2EraseOpcode;
  //
  // 9th dword
  //
  UINT8  SectorType3Size;
  UINT8  SectorType3EraseOpcode;
  UINT8  SectorType4Size;
  UINT8  SectorType4EraseOpcode;
  """
TypeMap = {
    'UINT8' : 'c_ubyte',
    'UINT32' : 'c_uint'
}
  
for Line in Str.splitlines():
    Line = Line.strip()
    if Line.startswith ('//') or len (Line) == 0:
        Line = Line.replace ('//', '#')
        print Line
    else:
        # bit field 
        if Line.find('[') >= 0:            
            Line = Line.replace ('[', ' ').replace(']',' ')
            Type  = Line.split()[0]
            Name  = Line.split()[1]
            Len   = Line.split()[2]
            print '("%s", %s * %d),' % (Name, TypeMap[Type], int(Len))        
        
        elif Line.find (':') >= 0: 
            Type  = Line.split(':')[0].split()[0]
            Name  = Line.split(':')[0].split()[1]
            Value = Line.split(':')[1].split(';')[0]
            print '("%s", %s, %d),' % (Name, TypeMap[Type], int(Value))
        else:
            Type  = Line.split()[0]
            Name  = Line.split()[1].split(';')[0]
            print '("%s", %s),' % (Name, TypeMap[Type])
            
    
    
  