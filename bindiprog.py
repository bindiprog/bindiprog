import Adafruit_GPIO.FT232H as FT232H
import binascii
import time
import os 
import ftdi1.ftdi1 as ftdi
from ctypes import Structure, c_uint, c_ushort, c_uint8, c_ubyte, sizeof
import ctypes
import struct
import xml.etree.ElementTree as ET
 
# Temporarily disable FTDI serial drivers.
FT232H.use_FT232H()
 
# Find the first FT232H device.
ft232h = FT232H.FT232H(0x0403, 0x6014)

 
ft232h._check(ftdi.read_data_set_chunksize, 65536)
ft232h._check(ftdi.write_data_set_chunksize, 65536)
 
# Create a SPI interface from the FT232H using pin 4 cs as chip select.
# Use a clock speed of 25mhz, SPI mode 0, and most significant bit first.

class SPI_SFDP_HEADER(Structure):
    _pack_ = 1
    _fields_ = [("SfdpSignature",      c_uint), 
                ("SfdpMinorRevNum",    c_ubyte), 
                ("SfdpMajorRevNum",    c_ubyte),
                ("ParameterHeaderNum", c_ubyte), 
                ("Reserve0",           c_ubyte)
               ]

class SPI_SFDP_PARAMETER_HEADER(Structure):
    _pack_ = 1
    _fields_ = [("ParameterIdLSB",            c_ubyte), 
                ("ParameterTableMinorRev",    c_ubyte), 
                ("ParameterTableMajorRev",    c_ubyte),
                ("ParameterTableLen",         c_ubyte), 
                ("ParameterTablePointer",     c_ubyte * 3), 
                ("ParameterIdMSB",            c_ubyte)
               ]
               
class SPI_SFDP_JEDEC_PARAMETER(Structure):
    _pack_ = 1
    _fields_ = [
#
# 1th dword
#
("BlockEraseSizes", c_ubyte, 2),
("WriteGranularity", c_ubyte, 1),
("VolatitleStatusBit", c_ubyte, 1),
("WriteEnableOpcodeSelect", c_ubyte, 1),
("Reserve0", c_ubyte, 3),
("EraseOpcode", c_ubyte),
("FastRead112Support", c_ubyte, 1),
("AddressBytes", c_ubyte, 2),
("DTRSupport", c_ubyte, 1),
("FastRead122Support", c_ubyte, 1),
("FastRead144Support", c_ubyte, 1),
("FastRead114Support", c_ubyte, 1),
("Reserve1", c_ubyte, 1),
("Reserve2", c_ubyte),
#
# 2nd dword
#
("FlashMemoryDensity", c_uint),
#
# 3rd dword
#
("FastRead144DummyCycle", c_ubyte, 5),
("FastRead144ModeBit", c_ubyte, 3),
("FastRead144OpCode", c_ubyte),
("FastRead114DummyCycle", c_ubyte, 5),
("FastRead114ModeBit", c_ubyte, 3),
("FastRead114OpCode", c_ubyte),
#
# 4th dword
#
("FastRead112DummyCycle", c_ubyte, 5),
("FastRead112ModeBit", c_ubyte, 3),
("FastRead112OpCode", c_ubyte),
("FastRead122DummyCycle", c_ubyte, 5),
("FastRead122ModeBit", c_ubyte, 3),
("FastRead122OpCode", c_ubyte),
#
# 5th dword
#
("FastRead222Support", c_ubyte, 1),
("Reserve3", c_ubyte, 3),
("FastRead444Support", c_ubyte, 1),
("Reserve4", c_ubyte, 3),
("Reserve5", c_ubyte * 3),
#
# 6th dword
#
("Reserve6", c_ubyte * 2),
("FastRead222DummyCycle", c_ubyte, 5),
("FastRead222ModeBit", c_ubyte, 3),
("FastRead222OpCode", c_ubyte),
#
# 7th dword
#
("Reserve7", c_ubyte * 2),
("FastRead444DummyCycle", c_ubyte, 5),
("FastRead444ModeBit", c_ubyte, 3),
("FastRead444OpCode", c_ubyte),
#
# 8th dword
#
("SectorType1Size", c_ubyte),
("SectorType1EraseOpcode", c_ubyte),
("SectorType2Size", c_ubyte),
("SectorType2EraseOpcode", c_ubyte),
#
# 9th dword
#
("SectorType3Size", c_ubyte),
("SectorType3EraseOpcode", c_ubyte),
("SectorType4Size", c_ubyte),
("SectorType4EraseOpcode", c_ubyte),
    ]

SFDP_SIGNATURE = 0x50444653
    
BASIC_SPI_PROTOCOL_ID = 0xFF00               

SPI_SR_WIP = (0x01 << 0)
SPI_SR_WEL = (0x01 << 1)
SPI_SR_AAI = (0x01 << 6)

JEDEC_WRSR = 0x01
JEDEC_RDSR = 0x05
JEDEC_WREN = 0x06
JEDEC_READ = 0x03
JEDEC_RDID = 0x9F
JEDEC_SFDP = 0x5A
JEDEC_ERASE = 0x20 
JEDEC_BYTE_PROGRAM = 0x02

# Chip Erase 0x60 is supported by Macronix/SST chips. 
# Chip Erase 0x62 is supported by Atmel AT25F chips. 
# Chip Erase 0xc7 is supported by SST/ST/EON/Macronix chips. 
JEDEC_CE_62 = 0x62
JEDEC_CE_60 = 0x60
JEDEC_CE_C7 = 0xC7

class SPI_FLASH(): 

    def __init__(self, spi):    
        self.spi = spi
        self.JedecParameter         = None
        self.FlashMemoryDensity     = 0
        self.Erase4kOpcode          = 0x00
        
        # JEDEC Basic Flash Parameter Table: 16th DWORD
        # Enter 4-Byte Addressing
        # This field defines the supported methods to enter 4-byte addressing mode or to use an extended 
        # address register with 3-byte addressing to access memory above 16 MBytes.
        # xxxx_xxx1b: issue instruction B7h (preceding write enable not required)
        # xxxx_xx1xb: issue write enable instruction 06h, then issue instruction B7h
        
        # Exit 4-Byte Addressing
        # xx_xxxx_xxx1b: issue instruction E9h to exit 4-Byte address mode (write enable instruction 06h is not required)
        # xx_xxxx_xx1xb: issue write enable instruction 06h, then issue instruction E9h to exit 4-Byte address mode        
        
        self.ReadStatusRegOpCode      = JEDEC_RDSR 
        self.WriteStatisRegOpcode     = JEDEC_WRSR
        self.ChipEraseOpCode          = JEDEC_CE_C7
        self.ProgramOpCode            = JEDEC_BYTE_PROGRAM
                                      
        self.Enter4ByteAddressing     = 0x00
        self.Exit4ByteAddressing      = 0x00
        self.SectorEraseOpcode        = 0x00
                                      
        self.BlockSizeInByte          = 0 
        self.SectorSizeInByte         = 0 
        self.MaxErasableSegmentInByte = 0

        self.dump_sfdp()
        self.search_chip_db(self.read_rdid())

    def read_rdid(self):
        response = self.spi.transfer([JEDEC_RDID, 0x00, 0x00, 0x00])
        rdid = (response[1] << 16) + (response[2] << 8) + (response[3])
        return rdid 
        
    def read_sfdp(self, Addr, Length): 
        Buf = [JEDEC_SFDP, 0x00, 0x00, 0x00, 0x00] + [0x00] * Length
        Buf[1] = Addr >> 16 & 0xFF
        Buf[2] = Addr >>  8 & 0xFF
        Buf[3] = Addr >>  0 & 0xFF
        Response = self.spi.transfer(Buf)[5:]        
        return Response 
        
    def search_chip_db (self, id):
        print hex(id)
        tree = ET.parse('ChipInfoDb.dedicfg')
        root = tree.getroot()
        result = []
        for child in root[0]:
            if child.tag != 'Chip':
                continue 
            if "IDNumber" not in child.attrib or child.attrib['IDNumber'] != "3":
                continue 
            if "JedecDeviceID" not in child.attrib or int(child.attrib["JedecDeviceID"], 0) != id: 
                continue
            
            self.FlashMemoryDensity = int(child.attrib["ChipSizeInKByte"], 0) * 1024
            if child.attrib["Class"] in ["EN25QHxx_Large", "MX25Lxxx_Large", "W25Pxx_Large", "W25Pxx"]:
                self.Enter4ByteAddressing = 1 << 0 
                self.Exit4ByteAddressing  = 1 << 0
                self.Erase4kOpcode        = 0x20
                self.SectorEraseOpcode    = 0xD8                
                                
                if 'BlockSizeInByte' in child.attrib:
                    self.BlockSizeInByte = int(child.attrib["BlockSizeInByte"], 0)
                if 'SectorSizeInByte' in child.attrib:
                    self.BlockSizeInByte = int(child.attrib["SectorSizeInByte"], 0)
                self.MaxErasableSegmentInByte = max(self.SectorSizeInByte, self.BlockSizeInByte)
                result.append (child)                 
            else:
                continue 
            
        else:
            if len (result) == 0:
                raise ValueError ("Chip is not found") 
            
    
    def dump_sfdp(self): 
        SpiHeader = SPI_SFDP_HEADER.from_buffer (self.read_sfdp(0, sizeof(SPI_SFDP_HEADER)))
        
        if SpiHeader.SfdpSignature != SFDP_SIGNATURE:
            print "SFDP is not found"
            return 
            
        
        print "SFDP %d.%d" % (SpiHeader.SfdpMajorRevNum, SpiHeader.SfdpMinorRevNum)
        print "Number of Parameter Headers %d"    % (SpiHeader.ParameterHeaderNum + 1)
        
        for Index in range (0, SpiHeader.ParameterHeaderNum + 1):
            OffsetParamterHdr = sizeof(SPI_SFDP_HEADER) + sizeof (SPI_SFDP_PARAMETER_HEADER) * Index 
            Buffer = self.read_sfdp(OffsetParamterHdr, sizeof(SPI_SFDP_PARAMETER_HEADER))
            ParamterHdr = SPI_SFDP_PARAMETER_HEADER.from_buffer (Buffer)    
            
            ParameterId = (ParamterHdr.ParameterIdMSB << 8) +  ParamterHdr.ParameterIdLSB
            print "ParamterHdr ID  0x%04X" % ParameterId
            print "ParamterHdr Ver %d.%d" % (ParamterHdr.ParameterTableMajorRev, ParamterHdr.ParameterTableMinorRev)
            print "ParamterHdr Len %d"    % ParamterHdr.ParameterTableLen
            PTP = ParamterHdr.ParameterTablePointer[0] + (ParamterHdr.ParameterTablePointer[1] << 8) + (ParamterHdr.ParameterTablePointer[2] << 16)
            print "PTP %x" % PTP
            
            if ParameterId == BASIC_SPI_PROTOCOL_ID:
                Buffer = self.read_sfdp(PTP, sizeof(SPI_SFDP_JEDEC_PARAMETER))
                self.JedecParameter = SPI_SFDP_JEDEC_PARAMETER.from_buffer (Buffer)
                if (ParamterHdr.ParameterTableLen * 4) < sizeof(SPI_SFDP_JEDEC_PARAMETER):
                    ctypes.memset(ctypes.addressof(self.JedecParameter), ParamterHdr.ParameterTableLen * 4, sizeof(SPI_SFDP_JEDEC_PARAMETER) - (ParamterHdr.ParameterTableLen * 4))
                    print "BASIC_SPI_PROTOCOL length is %d, and less than sizeof(SPI_SFDP_JEDEC_PARAMETER) %d" % (ParamterHdr.ParameterTableLen * 4, sizeof(SPI_SFDP_JEDEC_PARAMETER))
                
            
    def flash_read(self, Addr, Length):
        Cmd = [JEDEC_READ, 0x00, 0x00, 0x00]
        
        Cmd[1] = (Addr >> 16) & 0xFF
        Cmd[2] = (Addr >>  8) & 0xFF
        Cmd[3] = (Addr >>  0) & 0xFF

        Response = []
        
        self.spi._assert_cs()        
        self.spi.write (Cmd, auto_cs = False)         
        
        if (Length / 65536) > 0:
            for Count in xrange (0, Length, 65536):
                Response.append(self.spi.read(65536, auto_cs = False))

        if (Length % 65536) != 0:
            Response.append(self.spi.read(Length % 65536, auto_cs = False))

        self.spi._deassert_cs()      
        
        Result = bytearray()
        for Data in Response:
            Result += Data
            
        return Result

    def flash_write_enable (self):
        self.spi.write ([JEDEC_WREN])
        
    def flash_wait(self):
        Ret = self.spi.transfer ([JEDEC_RDSR, 0x00])[1]
        while (Ret & SPI_SR_WIP):   
            time.sleep(0.001)
            Ret = self.spi.transfer ([JEDEC_RDSR, 0x00])[1]    
            
        
    def flash_block_erase (self, Addr, BlockLen): 
        if self.JedecPParameter.EraseOpcode == 0xFF:
            raise ValueError ("Unsupport 4Kbytes erase opcode")
        
        Cmd = [self.JedecPParameter.EraseOpcode, 0x00, 0x00, 0x00]
        for Index in xrange (0, BlockLen):            
            Cmd[1] = (Addr >> 16) & 0xFF
            Cmd[2] = (Addr >>  8) & 0xFF
            Cmd[3] = (Addr >>  0) & 0xFF
            Addr = Addr + 4096
            self.flash_write_enable ()
            self.spi.write (Cmd)
            self.flash_wait()
            
    def flash_program (self, Addr, Buffer):
        if (Addr % 4096) != 0:
            raise ValueError("program address %x doesn't 4k align !!!")
        Length = len (Buffer) 
        if (Length % 4096) != 0:
            raise ValueError("program buffer length %x doesn't 4096 align !!!")
 
        Source = self.flash_read (Addr, Length)
        BaseAddress = Addr
        for Offset in xrange (0, Length, 4096): 
            Ret = list(set(Source[Offset:Offset+4096]))
            if len(Ret) != 1 or Ret[0] != 0xFF:
                self.flash_block_erase (BaseAddress + Offset, 1)
        
            for Offset in xrange (0, Length, 256): 
                Cmd = [JEDEC_BYTE_PROGRAM, 0, 0, 0] 
                Cmd[1] = ((BaseAddress + Offset) >> 16) & 0xFF
                Cmd[2] = ((BaseAddress + Offset) >>  8) & 0xFF
                Cmd[3] = ((BaseAddress + Offset) >>  0) & 0xFF
                self.flash_write_enable ()
                self.spi.write (bytearray(Cmd) + Buffer[Offset: Offset+256])
                self.flash_wait()
            
SpiFlash = SPI_FLASH(FT232H.SPI(ft232h, cs=3, max_speed_hz=30000000, mode=0, bitorder=FT232H.MSBFIRST))

print "RDID 0x%06X" % SpiFlash.read_rdid()
print "ROM size %x" % SpiFlash.FlashMemoryDensity

if 0:
    start = time.time()
    SpiFlash.flash_block_erase (0, 2)    
    print time.time() - start

if 1:
    start = time.time()
    Result = SpiFlash.flash_read(0, SpiFlash.FlashMemoryDensity)
    print time.time() - start
    with open(os.path.join(os.getcwd(), 'output.bin'), 'wb') as f:
        f.write(Result)

if 0:
    start = time.time()
    Buffer = None
    with open(os.path.join(os.getcwd(), 'output2.bin'), 'rb') as f:
        Buffer = f.read()
    print time.time() - start
    
    start = time.time()
    SpiFlash.flash_program (0, Buffer)
    print time.time() - start

if 0:
    start = time.time()
    Result = SpiFlash.flash_read(0, pow(2,20) * 8)
    print time.time() - start
    with open(os.path.join(os.getcwd(), 'output.bin'), 'wb') as f:
        f.write(Result)
